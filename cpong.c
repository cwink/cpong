#include "include/game.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define WINDOW_TITLE "CPong"
#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 768

#define BACKGROUND_RED 0
#define BACKGROUND_GREEN 0
#define BACKGROUND_BLUE 0
#define BACKGROUND_ALPHA 255

typedef struct pong_t
{
  GAME_BASIC_ACTOR ball;
  GAME_BASIC_ACTOR paddle;
  GAME_BASIC_FONT font;
  unsigned int score;
  GAME_BOOL game_over;
  GAME_BOOL game_start;
  unsigned char padding[4];
} PONG;

void event (GAME *game, const SDL_Event *const event);

void post_event (GAME *game);

void integrate (GAME *game, const double time, const double delta_time);

void update (GAME *game);

void render (GAME *game);

void pong_ball_init (PONG *pong);

void pong_paddle_init (PONG *pong);

int
main (void)
{
  GAME game;
  PONG pong;

  pong_ball_init (&pong);
  pong_paddle_init (&pong);

  pong.font.location.x = 10.0;
  pong.font.location.y = 10.0;
  pong.font.dimension.width = 4;
  pong.font.dimension.height = 4;
  pong.font.color.red = 255;
  pong.font.color.green = 255;
  pong.font.color.blue = 255;
  pong.font.color.alpha = 255;

  pong.score = 0;

  pong.game_over = GAME_BOOL_FALSE;

  pong.game_start = GAME_BOOL_FALSE;

  if (game_init (&game) == GAME_BOOL_FALSE)
    {
      fprintf (stderr, "Failed to initialize game object.");
      return EXIT_FAILURE;
    }

  game.title = WINDOW_TITLE;
  game.dimension.width = WINDOW_WIDTH;
  game.dimension.height = WINDOW_HEIGHT;
  game.event = event;
  game.post_event = post_event;
  game.integrate = integrate;
  game.update = update;
  game.render = render;
  game.data = &pong;

  if (game_run (&game) == GAME_BOOL_FALSE)
    {
      fprintf (stderr, "Failed to run game.");
    }

  if (game.error != GAME_ERROR_NONE)
    {
      fprintf (stderr, "%s", game_error_string (&game));
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

void
event (GAME *game, const SDL_Event *const event)
{
  PONG *pong = (PONG *)game->data;

  switch (event->type)
    {
    case SDL_KEYDOWN:
      switch (event->key.repeat)
        {
        case 0:
          if (event->key.keysym.scancode == SDL_SCANCODE_ESCAPE)
            {
              game->running = GAME_BOOL_FALSE;
            }

          if (pong->game_over == GAME_BOOL_TRUE
              && event->key.keysym.scancode == SDL_SCANCODE_SPACE)
            {
              pong->game_over = GAME_BOOL_FALSE;

              pong_ball_init (pong);
              pong_paddle_init (pong);

              pong->score = 0;
            }

          if (pong->game_start == GAME_BOOL_FALSE
              && event->key.keysym.scancode == SDL_SCANCODE_SPACE)
            {
              pong->game_start = GAME_BOOL_TRUE;
            }

          break;
        default:
          break;
        }
      break;
    }
}

void
post_event (GAME *game)
{
  PONG *pong = (PONG *)game->data;

  if (pong->game_over == GAME_BOOL_TRUE || pong->game_start == GAME_BOOL_FALSE)
    {
      return;
    }

  pong->paddle.velocity.x = 0.0;

  if (SDL_GetKeyboardState (NULL)[SDL_SCANCODE_A])
    {
      pong->paddle.velocity.x = -1.0;
    }
  else if (SDL_GetKeyboardState (NULL)[SDL_SCANCODE_D])
    {
      pong->paddle.velocity.x = 1.0;
    }
}

void
integrate (GAME *game, const double time, const double delta_time)
{
  PONG *pong = (PONG *)game->data;

  (void)time;

  if (pong->game_over == GAME_BOOL_TRUE || pong->game_start == GAME_BOOL_FALSE)
    {
      return;
    }

  game_basic_actor_move (&pong->ball, delta_time);

  game_basic_actor_move (&pong->paddle, delta_time);
}

void
update (GAME *game)
{
  PONG *pong = (PONG *)game->data;

  if (pong->game_over == GAME_BOOL_TRUE || pong->game_start == GAME_BOOL_FALSE)
    {
      return;
    }

  if (pong->ball.location.x <= 0.0)
    {
      pong->ball.velocity.x = fabs (pong->ball.velocity.x);
    }

  if (pong->ball.location.x + (double)pong->ball.dimension.width
      >= (double)game->dimension.width)
    {
      pong->ball.velocity.x = -1.0 * fabs (pong->ball.velocity.x);
    }

  if (pong->ball.location.y <= 0.0)
    {
      pong->ball.velocity.y = fabs (pong->ball.velocity.y);
    }

  if (pong->paddle.location.x <= 0.0)
    {
      pong->paddle.location.x = 0.0;
    }

  if (pong->paddle.location.x + (double)pong->paddle.dimension.width
      >= (double)game->dimension.width)
    {
      pong->paddle.location.x = (double)game->dimension.width
                                - (double)pong->paddle.dimension.width;
    }

  if (pong->ball.location.y + (double)pong->ball.dimension.height
          >= pong->paddle.location.y
      && pong->ball.location.x + (double)pong->ball.dimension.width
             >= pong->paddle.location.x
      && pong->ball.location.x
             <= pong->paddle.location.x + (double)pong->paddle.dimension.width)
    {
      double ball_center_x = 0.0;
      double ball_center_y = 0.0;
      double paddle_center_x = 0.0;
      double paddle_center_y = 0.0;
      double hit_x = 0.0;
      double hit_y = 0.0;
      double hit_mag = 0.0;

      pong->ball.location.y = pong->paddle.location.y
                              - (double)pong->ball.dimension.height - 1.0;

      ball_center_x
          = pong->ball.location.x + (double)pong->ball.dimension.width / 2.0;
      ball_center_y
          = pong->ball.location.y + (double)pong->ball.dimension.height / 2.0;
      paddle_center_x = pong->paddle.location.x
                        + (double)pong->paddle.dimension.width / 2.0;
      paddle_center_y = pong->paddle.location.y;
      hit_x = ball_center_x - paddle_center_x;
      hit_y = ball_center_y - paddle_center_y;
      hit_mag = sqrt (hit_x * hit_x + hit_y * hit_y);

      pong->ball.velocity.x = hit_x / hit_mag;
      pong->ball.velocity.y = hit_y / hit_mag;

      pong->ball.speed += 30.0;
      pong->paddle.speed += 30.0;
      pong->score += 5;
    }
  else if (pong->ball.location.y + (double)pong->ball.dimension.height
           >= (double)game->dimension.height)
    {
      pong->game_over = GAME_BOOL_TRUE;
    }
}

void
render (GAME *game)
{
  char buffer[32] = { '\0' };
  PONG *pong = (PONG *)game->data;
  GAME_COLOR background_color = { BACKGROUND_RED, BACKGROUND_GREEN,
                                  BACKGROUND_BLUE, BACKGROUND_ALPHA };
  if (pong->game_start == GAME_BOOL_FALSE)
    {
      GAME_BASIC_FONT font_press;
      GAME_BASIC_FONT font_title;

      font_press.dimension.width = 5;
      font_press.dimension.height = 5;
      font_press.location.x
          = WINDOW_WIDTH / 2.0
            - (double)font_press.dimension.width * 8.0 * 21.0 / 2.0;
      font_press.location.y
          = WINDOW_HEIGHT / 2.0
            - (double)font_press.dimension.height * 8.0 / 2.0;
      font_press.color.red = 0;
      font_press.color.green = 255;
      font_press.color.blue = 255;
      font_press.color.alpha = 255;

      font_title.dimension.width = 16;
      font_title.dimension.height = 16;
      font_title.location.x
          = WINDOW_WIDTH / 2.0
            - (double)font_title.dimension.width * 8.0 * 5.0 / 2.0;
      font_title.location.y
          = WINDOW_HEIGHT / 4.0
            - (double)font_title.dimension.height * 8.0 / 2.0;
      font_title.color.red = 0;
      font_title.color.green = 255;
      font_title.color.blue = 0;
      font_title.color.alpha = 255;

      game_basic_font_draw (game, &font_press, "Press any key to play");
      game_basic_font_draw (game, &font_title, "CPong");

      return;
    }

  if (pong->game_over == GAME_BOOL_TRUE)
    {
      GAME_BASIC_FONT font;

      font.dimension.width = 12;
      font.dimension.height = 12;
      font.location.x = WINDOW_WIDTH / 2.0
                        - (double)font.dimension.width * 8.0 * 9.0 / 2.0;
      font.location.y
          = WINDOW_HEIGHT / 2.0 - (double)font.dimension.height * 8.0 / 2.0;
      font.color.red = 255;
      font.color.green = 255;
      font.color.blue = 0;
      font.color.alpha = 255;

      game_basic_font_draw (game, &font, "GAME OVER");

      return;
    }

  game_clear_screen (game, &background_color);

  if (game->error != GAME_ERROR_NONE)
    {
      return;
    }

  game_basic_actor_draw (game, &pong->ball);

  if (game->error != GAME_ERROR_NONE)
    {
      return;
    }

  game_basic_actor_draw (game, &pong->paddle);

  if (game->error != GAME_ERROR_NONE)
    {
      return;
    }

  sprintf (buffer, "Score: %u", pong->score);

  game_basic_font_draw (game, &pong->font, buffer);

  if (game->error != GAME_ERROR_NONE)
    {
      return;
    }
}

void
pong_ball_init (PONG *pong)
{
  pong->ball.dimension.width = 16;
  pong->ball.dimension.height = 16;
  pong->ball.location.x
      = WINDOW_WIDTH / 2.0 / 2.0 - pong->ball.dimension.width / 2.0;
  pong->ball.location.y
      = WINDOW_HEIGHT / 2.0 / 2.0 - pong->ball.dimension.height / 2.0;
  pong->ball.velocity.x = 1.0;
  pong->ball.velocity.y = 1.0;
  pong->ball.speed = 300.0;
  pong->ball.color.red = 0;
  pong->ball.color.green = 255;
  pong->ball.color.blue = 0;
  pong->ball.color.alpha = 255;
  pong->ball.filled = GAME_BOOL_TRUE;
}

void
pong_paddle_init (PONG *pong)
{
  pong->paddle.dimension.width = 96;
  pong->paddle.dimension.height = 16;
  pong->paddle.location.x
      = WINDOW_WIDTH / 2.0 - pong->ball.dimension.width / 2.0;
  pong->paddle.location.y = WINDOW_HEIGHT - pong->ball.dimension.height;
  pong->paddle.velocity.x = 0.0;
  pong->paddle.velocity.y = 0.0;
  pong->paddle.speed = 300.0;
  pong->paddle.color.red = 255;
  pong->paddle.color.green = 0;
  pong->paddle.color.blue = 0;
  pong->paddle.color.alpha = 255;
  pong->paddle.filled = GAME_BOOL_TRUE;
}
