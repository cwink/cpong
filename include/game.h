#ifndef GAME_H
#define GAME_H

#include <SDL2/SDL.h>

#define GAME_PI 3.14159265359

typedef enum game_bool_t
{
  GAME_BOOL_FALSE = 0,
  GAME_BOOL_TRUE
} GAME_BOOL;

typedef enum game_error_t
{
  GAME_ERROR_NONE = 0,
  GAME_ERROR_SDL,
  GAME_ERROR_MAX
} GAME_ERROR;

typedef struct game_point_t
{
  double x;
  double y;
} GAME_POINT;

typedef struct game_dimension_t
{
  size_t width;
  size_t height;
} GAME_DIMENSION;

typedef struct game_color_t
{
  unsigned char red;
  unsigned char green;
  unsigned char blue;
  unsigned char alpha;
} GAME_COLOR;

typedef struct game_basic_actor_t
{
  GAME_POINT location;
  GAME_POINT velocity;
  double speed;
  GAME_DIMENSION dimension;
  GAME_COLOR color;
  GAME_BOOL filled;
} GAME_BASIC_ACTOR;

typedef struct game_basic_font_t
{
  GAME_POINT location;
  GAME_DIMENSION dimension;
  GAME_COLOR color;
  unsigned char padding[4];
} GAME_BASIC_FONT;

typedef struct game_t GAME;

typedef void (*GAME_EVENT) (GAME *game, const SDL_Event *const event);

typedef void (*GAME_POST_EVENT) (GAME *game);

typedef void (*GAME_INTEGRATE) (GAME *game, const double time,
                                const double delta_time);

typedef void (*GAME_UPDATE) (GAME *game);

typedef void (*GAME_RENDER) (GAME *game);

struct game_t
{
  char *title;
  SDL_Window *window;
  SDL_Renderer *renderer;
  GAME_DIMENSION dimension;
  double delta_time;
  GAME_EVENT event;
  GAME_POST_EVENT post_event;
  GAME_INTEGRATE integrate;
  GAME_UPDATE update;
  GAME_RENDER render;
  void *data;
  GAME_BOOL vsync;
  GAME_BOOL opengl;
  GAME_BOOL accelerated;
  GAME_BOOL running;
  GAME_ERROR error;
  unsigned char padding[4];
};

const char *game_error_string (const GAME *const game);

GAME_BOOL game_init (GAME *game);

GAME_BOOL game_run (GAME *game);

double game_random (const double min, const double max);

GAME_BOOL game_clear_screen (GAME *game, const GAME_COLOR *const color);

GAME_BOOL game_basic_actor_draw (GAME *game,
                                 const GAME_BASIC_ACTOR *const actor);

GAME_BOOL game_basic_actor_move (GAME_BASIC_ACTOR *actor,
                                 const double delta_time);

GAME_BOOL game_basic_actor_collision (const GAME_BASIC_ACTOR *const left,
                                      const GAME_BASIC_ACTOR *const right);

GAME_BOOL game_basic_font_draw (GAME *game, const GAME_BASIC_FONT *const font,
                                const char *const text);

#endif /* GAME_H */
